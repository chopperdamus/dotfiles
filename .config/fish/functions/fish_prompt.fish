function fish_prompt --description 'Write out the prompt'
	if test -z $WINDOW
        printf '%s%s%s@%s%s%s%s%s> ' (set_color af5fff) (whoami) (set_color normal) (set_color d75f00) (prompt_hostname) (set_color $fish_color_cwd) (prompt_pwd) (set_color normal)
    else
        printf '%s%s%s@%s%s%s(%s)%s%s%s> ' (set_color af5fff) (whoami) (set_color normal) (set_color d75f00) (prompt_hostname) (set_color white) (echo $WINDOW) (set_color $fish_color_cwd) (prompt_pwd) (set_color normal)
    end
end
